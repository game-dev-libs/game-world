package stepmethods;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.events.CollisionEvent;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.gameworld.object.MovingGameWorldObject;
import com.felixcool98.gameworld.stepmethods.GroundTouchStep;
import com.felixcool98.gameworld.stepmethods.TouchedGround;
import com.felixcool98.utility.values.Value;

public class GroundTouchTests {

	@Test
	public void testSendOnce() {
		GameWorld world = new GameWorld();
		
		world.create(new GameWorldObject(new Rectangle(1, 1)));
		
		MovingGameWorldObject object = new MovingGameWorldObject(new Rectangle(0, 4, 1, 1)) {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		object.addStepMethod(new GroundTouchStep(true));
		object.setYSpeed(-1);
		
		world.create(object);
		
		Value<Integer> value = new Value<Integer>(0);
		
		object.getManager().register(new Object() {
			@EventListener
			public void listen(TouchedGround event) {
				value.set(value.get()+1);
			}
		});
		
		for(int i = 0; i < 100; i++) {
			world.step(1);
		}
		
		assertEquals(1, (int)value.get());
	}
	@Test
	public void testSendAlways() {
		GameWorld world = new GameWorld();
		
		world.create(new GameWorldObject(new Rectangle(1, 1)));
		
		MovingGameWorldObject object = new MovingGameWorldObject(new Rectangle(0, 4, 1, 1)) {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		object.data().stepData().add(new GroundTouchStep());
		object.setYSpeed(-1);
		
		world.create(object);
		
		Value<Integer> value = new Value<Integer>(0);
		
		object.getManager().register(new Object() {
			@EventListener
			public void listen(TouchedGround event) {
				value.set(value.get()+1);
			}
		});
		
		for(int i = 0; i < 100; i++) {
			world.step(1);
		}
		
		assertTrue(value.get() > 0);
	}
}
