package steptest;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.step.Step;
import com.felixcool98.step.StepMethod;
import com.felixcool98.step.WorldStep;
import com.felixcool98.step.utils.Tuple2;

public class StepTests {
	@Test
	public void testParallel() {
		WorldStep step = new WorldStep();
		
		List<GameWorldObject> objects = new LinkedList<>();
		
		for(int i = 0; i < 100; i++) 
			objects.add(new GameWorldObjectTestParallel(i));
		
		for(int i = 0; i < 10; i++)
			step(step, objects);
	}
	@Test
	public void testNonParallel() {
		WorldStep step = new WorldStep();
		
		List<GameWorldObject> objects = new LinkedList<>();
		
		for(int i = 0; i < 10; i++) 
			objects.add(new GameWorldObjectTestNonParallel(i));
		
		for(int i = 0; i < 10; i++)
			step(step, objects);
	}
	
	private void step(WorldStep step, List<GameWorldObject> stepObjects) {
		List<Tuple2<Object, StepMethod>> objects = new LinkedList<Tuple2<Object,StepMethod>>();
		
		for(GameWorldObject object : stepObjects) {
			for(StepMethod method : object.data().stepData().getStepMethods()) {
				objects.add(new Tuple2<Object, StepMethod>(object, method));
			}
		}
		
		step.step(objects, 100);
	}

	
	public static class GameWorldObjectTestParallel extends GameWorldObject{
		private int id;
		
		
		public GameWorldObjectTestParallel(int id) {
			super(new Rectangle(0, 0, 0, 0), "default");
			
			this.id = id;
		}
		
		
		@Step(parallel = true)
		public void step(float delta) {
			delta += id;
		}
	}
	public static class GameWorldObjectTestNonParallel extends GameWorldObject{
		private int id;
		
		
		public GameWorldObjectTestNonParallel(int id) {
			super(new Rectangle(0, 0, 0, 0), "default");
			
			this.id = id;
		}
		
		
		@Step(parallel = false)
		public void step(float delta) {
			delta += id;
		}
	}
}
