package gameworldobject.moveable;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameObject;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.events.CollisionEvent;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.gameworld.object.MovingGameWorldObject;
import com.felixcool98.step.Step;
import com.felixcool98.worlds.bruteforce.BruteForceWorld;

@RunWith(JUnit4.class)
public class MovementMultipleWorlds {
	@Test
	public void testMoveAgainstOther() {
		int steps = 10;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		obj.setSpeed(1.01f, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(2, 0, 1, 1), "static", "move"));
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(2, obj.getShape().getAABB().getRight(), 0.001f);
	}
	@Test
	public void testSpawnsInOther() {
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		obj.setSpeed(1.01f, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(0, 0, 1, 1), "static", "move"));
		
		for(int i = 0; i < 10; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(1);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(0, obj.getShape().getAABB().getLeft(), 0.0001f);
	}
	@Test
	public void moveAgainstOver() {
		int steps = 10;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 1, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		obj.setSpeed(1, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(0, 0, 100, 1), "static", "move"));
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*(float)steps*delta, obj.getShape().getAABB().getLeft(), 0.001f);
	}
	
	
	@Test
	public void testMoveAgainstOtherIgnoreOtherWorlds() {
		int steps = 10;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				if(event.other.getWorldName().equals("static"))
					return;
				
				event.consume();
			}
		};
		
		obj.setSpeed(1.01f, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(2, 0, 1, 1), "static", "move"));
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*steps*delta, obj.getShape().getAABB().getLeft(), 0.001f);
	}
	@Test
	public void testSpawnsInOtherIgnoreOtherWorlds() {
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				if(event.other.getWorldName().equals("static"))
					return;
				
				event.consume();
			}
		};
		
		obj.setSpeed(1.01f, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(0, 0, 1, 1), "static", "move"));
		
		for(int i = 0; i < 10; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(1);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*10f, obj.getShape().getAABB().getLeft(), 0.0001f);
	}
	@Test
	public void moveAgainstOverIgnoreOtherWorlds() {
		int steps = 10;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 1, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				if(event.other.getWorldName().equals("static"))
					return;
				
				event.consume();
			}
		};
		
		obj.setSpeed(1, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(0, 0, 100, 1), "static", "move"));
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*(float)steps*delta, obj.getShape().getAABB().getLeft(), 0.001f);
	}
	
	@Test
	public void testMoveAgainstOtherDestroyOther() {
		int steps = 10;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				if(event.other.getWorldName().equals("static"))
					return;
				
				event.consume();
			}
			@Step
			public void destroyNearby(float delta) {
				for(GameObject<?, ?, ?> object : getIntersections()) {
					object.destroy();
				}
			}
		};
		
		obj.setSpeed(1.01f, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(2, 0, 1, 1), "static", "move"));
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*steps*delta, obj.getShape().getAABB().getLeft(), 0.001f);
	}
	@Test
	public void testSpawnsInOtherDestroyOther() {
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				if(event.other.getWorldName().equals("static"))
					return;
				
				event.consume();
			}
			@Step
			public void destroyNearby(float delta) {
				for(GameObject<?, ?, ?> object : getIntersections()) {
					object.destroy();
				}
			}
		};
		
		obj.setSpeed(1.01f, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(0, 0, 1, 1), "static", "move"));
		
		for(int i = 0; i < 10; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(1);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*10f, obj.getShape().getAABB().getLeft(), 0.0001f);
	}
	@Test
	public void moveAgainstOverDestroyOther() {
		int steps = 10;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		world.add("move", new BruteForceWorld());
		world.add("static", new BruteForceWorld());
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 1, 1, 1), "move", "static") {
			@EventListener
			public void collision(CollisionEvent event) {
				if(event.other.getWorldName().equals("static"))
					return;
				
				event.consume();
			}
			@Step
			public void destroyNearby(float delta) {
				for(GameObject<?, ?, ?> object : getIntersections()) {
					object.destroy();
				}
			}
		};
		
		obj.setSpeed(1, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(0, 0, 100, 1), "static", "move"));
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*(float)steps*delta, obj.getShape().getAABB().getLeft(), 0.001f);
	}
}
