package gameworldobject.moveable;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.events.CollisionEvent;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.gameworld.object.MovingGameWorldObject;

@RunWith(JUnit4.class)
public class MovementTests {
	@Test
	public void testAlone() {
		int steps = 10;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1));
		
		world.create(obj);
		
		obj.setSpeed(1, 0);
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*(float)steps*delta, obj.getShape().getAABB().getLeft(), 0.001f);
	}
	@Test
	public void testMoveAgainstOtherX() {
		int steps = 100;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1)) {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		obj.setSpeed(1.01f, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(2, 0, 1, 1)));
		
		float maxPosDiff = delta*obj.getXSpeed();
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			float diff = obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft();

			assertTrue(Math.abs(diff) <= Math.abs(maxPosDiff) + 0.0001f);
		}
		
		assertEquals(2, obj.getShape().getAABB().getRight(), 0.001f);
	}
	@Test
	public void testMoveAgainstOtherY() {
		int steps = 100;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 2, 1, 1)) {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		obj.setSpeed(0, -1.01f);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(1, 1)));
		
		float maxPosDiff = delta*obj.getYSpeed();
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			float diff = obj.getShape().getAABB().getBot()-begin.getAABB().getBot();

			assertTrue(Math.abs(diff) <= Math.abs(maxPosDiff) + 0.0001f);
		}
		
		assertEquals(1, obj.getShape().getAABB().getBot(), 0.001f);
	}
	@Test
	public void testSpawnsInOther() {
		GameWorld world = new GameWorld();
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1)) {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		obj.setSpeed(1.01f, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(0, 0, 1, 1)));
		
		for(int i = 0; i < 10; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(1);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(0, obj.getShape().getAABB().getLeft(), 0.0001f);
	}
	@Test
	public void moveAgainstOver() {
		int steps = 10;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 1, 1, 1)) {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		obj.setSpeed(1, 0);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(0, 0, 100, 1)));
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getLeft()-begin.getAABB().getLeft()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(obj.getXSpeed()*(float)steps*delta, obj.getShape().getAABB().getLeft(), 0.001f);
	}
	
	@Test
	public void moveOverLeftEdgeFromTop() {
		int steps = 30;
		float delta = 0.1f;
		
		GameWorld world = new GameWorld();
		
		MovingGameWorldObject obj = new MovingGameWorldObject(new Rectangle(0, 2, 1, 1)) {
			@EventListener
			public void collision(CollisionEvent event) {
				event.consume();
			}
		};
		
		float startY = obj.getShape().getAABB().getBot();
		
		obj.setSpeed(0, -1);
		
		world.create(obj);
		
		world.create(new GameWorldObject(new Rectangle(1, 0, 10, 1)));
		
		for(int i = 0; i < steps; i++) {
			Shape begin = obj.getShape().clone();
			
			world.step(delta);
			
			assertTrue(obj.getShape().getAABB().getBot()-begin.getAABB().getBot()-obj.getXSpeed() < 0.0001f);
		}
		
		assertEquals(startY + obj.getYSpeed()*(float)steps*delta, obj.getShape().getAABB().getBot(), 0.001f);
	}
}
