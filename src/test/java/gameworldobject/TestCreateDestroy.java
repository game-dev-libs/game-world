package gameworldobject;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.events.CreationEvent;
import com.felixcool98.gameworld.events.DestroyEvent;
import com.felixcool98.gameworld.object.GameWorldObject;

public class TestCreateDestroy {
	@Test
	public void test() {
		GameWorld world = new GameWorld();
		
		TestObject object = new TestObject();
		
		world.create(object);
		world.create(object);
		
		world.forceUpdate();
		
		assertEquals(1, object.created);
		
		object.destroy();
		object.destroy();
		
		world.forceUpdate();
		
		assertEquals(1, object.destroyed);
	}
	
	
	private static class TestObject extends GameWorldObject{
		private int created = 0;
		private int destroyed = 0;
		
		private TestObject() {
			super(new Rectangle(0, 0, 1, 1));
		}
		
		
		@EventListener
		public void created(CreationEvent event) {
			created++;
		}
		@EventListener
		public void destroyed(DestroyEvent event) {
			destroyed++;
		}
	}
}
