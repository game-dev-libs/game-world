package gameworldobject;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.gameworld.GameObject;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.object.GameWorldObject;

public class SurroundingTests {

	@Test
	public void testArray() {
		GameWorld world = new GameWorld();
		
		TestObject middle = new TestObject(new Rectangle(1, 1, 1, 1), "middle");
		
		world.create(new TestObject(new Rectangle(0, 0, 1, 1), "botleft"));
		world.create(new TestObject(new Rectangle(1, 0, 1, 1), "bot"));
		world.create(new TestObject(new Rectangle(2, 0, 1, 1), "botright"));
		
		world.create(new TestObject(new Rectangle(0, 1, 1, 1), "left"));
		world.create(middle);
		world.create(new TestObject(new Rectangle(2, 1, 1, 1), "right"));
		
		world.create(new TestObject(new Rectangle(0, 2, 1, 1), "topleft"));
		world.create(new TestObject(new Rectangle(1, 2, 1, 1), "top"));
		world.create(new TestObject(new Rectangle(2, 2, 1, 1), "topright"));
		
		world.forceUpdate();
		
		List<GameObject<?, ?, ?>>[][] array = middle.getSurroundingAsArray();
		
		assertEquals("botleft", ((TestObject) array[0][0].get(0)).name);
		assertEquals("bot", ((TestObject) array[1][0].get(0)).name);
		assertEquals("botright", ((TestObject) array[2][0].get(0)).name);
		
		assertEquals("left", ((TestObject) array[0][1].get(0)).name);
		assertEquals("right", ((TestObject) array[2][1].get(0)).name);
		
		assertEquals("topleft", ((TestObject) array[0][2].get(0)).name);
		assertEquals("top", ((TestObject) array[1][2].get(0)).name);
		assertEquals("topright", ((TestObject) array[2][2].get(0)).name);
	}
	@Test
	public void testDir() {
		GameWorld world = new GameWorld();
		
		TestObject middle = new TestObject(new Rectangle(1, 1, 1, 1), "middle");
		
		world.create(new TestObject(new Rectangle(0, 0, 1, 1), "botleft"));
		world.create(new TestObject(new Rectangle(1, 0, 1, 1), "bot"));
		world.create(new TestObject(new Rectangle(2, 0, 1, 1), "botright"));
		
		world.create(new TestObject(new Rectangle(0, 1, 1, 1), "left"));
		world.create(middle);
		world.create(new TestObject(new Rectangle(2, 1, 1, 1), "right"));
		
		world.create(new TestObject(new Rectangle(0, 2, 1, 1), "topleft"));
		world.create(new TestObject(new Rectangle(1, 2, 1, 1), "top"));
		world.create(new TestObject(new Rectangle(2, 2, 1, 1), "topright"));
		
		world.forceUpdate();
		
		assertTrue(middle.getSurrounding().isAll());
	}
	

	
	private static class TestObject extends GameWorldObject{
		private String name;
		
		
		private TestObject(Shape shape, String name) {
			super(shape);
			
			this.name = name;
		}
	}
}
