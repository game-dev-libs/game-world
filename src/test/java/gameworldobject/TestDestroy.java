package gameworldobject;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.step.Step;

public class TestDestroy {

	@Test
	public void test() {
		GameWorld world = new GameWorld();
		
		world.create(new TestObject());
		world.create(new TestObject());
		world.create(new TestObject());
		world.create(new TestObject());
		world.create(new TestObject());
		
		for(int i = 0; i < 1000; i++) {
			world.step(0.5f);
		}
	}

	
	public static class TestObject extends GameWorldObject{
		private float time;
		
		
		public TestObject() {
			super(new Rectangle(0, 0, 1, 1));
		}
		
		@Step
		public void step(float delta) {
			if(time > 10) 
				fail("step despite destroyed");
			
			time += delta;
			
			if(time > 10) 
				destroy();
		}
	}
}
