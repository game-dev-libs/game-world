package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import gameworldobject.SurroundingTests;
import gameworldobject.TestCreateDestroy;
import gameworldobject.TestDestroy;
import gameworldobject.moveable.MovementMultipleWorlds;
import gameworldobject.moveable.MovementTests;
import handlers.WorldBorderHandlerTests;
import stepmethods.GroundTouchTests;
import steptest.StepTests;

@RunWith(Suite.class)
@SuiteClasses({StepTests.class,
	SurroundingTests.class, TestDestroy.class,
	MovementTests.class, MovementMultipleWorlds.class,
	WorldBorderHandlerTests.class,
	GroundTouchTests.class,
	TestCreateDestroy.class})
public class AllTests {

}
