package handlers;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.events.handlers.WorldBorderHandler;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.gameworld.object.MovingGameWorldObject;

public class WorldBorderHandlerTests {

	@Test
	public void testCreation() {
		GameWorld world = new GameWorld();
		
		world.register(new WorldBorderHandler(new Rectangle(-5, -5, 10, 10)));
		
		GameWorldObject object = new GameWorldObject(new Rectangle(-10, -10, 1, 1));
		
		world.create(object);
		
		world.forceUpdate();
		
		assertEquals(-5, object.getShape().getAABB().getLeft(), 0.01f);
		assertEquals(-5, object.getShape().getAABB().getBot(), 0.01f);
	}
	@Test
	public void testMoving() {
		GameWorld world = new GameWorld();
		
		world.register(new WorldBorderHandler(new Rectangle(-5, -5, 10, 10)));
		
		MovingGameWorldObject object = new MovingGameWorldObject(new Rectangle(0, 0, 1, 1));
		
		object.setSpeed(-1, -1);
		
		world.create(object);
		
		world.step(100);
		
		assertEquals(-5, object.getShape().getAABB().getLeft(), 0.01f);
		assertEquals(-5, object.getShape().getAABB().getBot(), 0.01f);
		
		object.setSpeed(1, 1);
		world.step(100);
		
		assertEquals(5, object.getShape().getAABB().getRight(), 0.01f);
		assertEquals(5, object.getShape().getAABB().getTop(), 0.01f);
	}
}
