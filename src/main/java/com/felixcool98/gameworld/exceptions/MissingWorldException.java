package com.felixcool98.gameworld.exceptions;

public class MissingWorldException extends RuntimeException {
	private static final long serialVersionUID = 4300071581995407935L;

	
	public MissingWorldException(String message) {
		super(message);
	}
}
