package com.felixcool98.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.felixcool98.events.ConsumableEvent;
import com.felixcool98.events.ConsumableEvent.ConsumeableEventAdapter;

public class GWInputProcessor extends InputAdapter {
	private GameWorld world;
	private OrthographicCamera camera;
	
	
	public GWInputProcessor(GameWorld world, OrthographicCamera camera) {
		this.world = world;
		this.camera = camera;
	}
	
	
	public void setCamera(OrthographicCamera camera) {
		this.camera = camera;
	}
	
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		Vector3 vector = camera.unproject(new Vector3(screenX, screenY, 0));
		
		ConsumableEvent event = new TouchDown(vector.x, vector.y, button);
		
		return world.emit(event, vector.x, vector.y);
	}
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		Vector3 vector = camera.unproject(new Vector3(screenX, screenY, 0));
		
		ConsumableEvent event = new TouchUp(vector.x, vector.y, button);
		
		return world.emit(event, vector.x, vector.y);
	}
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		Vector3 vector = camera.unproject(new Vector3(screenX, screenY, 0));
		
		ConsumableEvent event = new TouchDragged(vector.x, vector.y);
		
		return world.emit(event, vector.x, vector.y);
	}
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		Vector3 vector = camera.unproject(new Vector3(screenX, screenY, 0));
		
		ConsumableEvent event = new MouseMoved(vector.x, vector.y);
		
		return world.emit(event, vector.x, vector.y);
	}
	@Override
	public boolean keyTyped(char character) {
		int screenX = Gdx.input.getX();
		int screenY = Gdx.input.getY();
		
		Vector3 vector = camera.unproject(new Vector3(screenX, screenY, 0));
		
		ConsumableEvent event = new KeyTyped(vector.x, vector.y, character);
		
		return world.emit(event, vector.x, vector.y);
	}
	@Override
	public boolean keyDown(int keycode) {
		int screenX = Gdx.input.getX();
		int screenY = Gdx.input.getY();
		
		Vector3 vector = camera.unproject(new Vector3(screenX, screenY, 0));
		
		ConsumableEvent event = new KeyDown(vector.x, vector.y, keycode);
		
		return world.emit(event, vector.x, vector.y);
	}
	@Override
	public boolean keyUp(int keycode) {
		int screenX = Gdx.input.getX();
		int screenY = Gdx.input.getY();
		
		Vector3 vector = camera.unproject(new Vector3(screenX, screenY, 0));
		
		ConsumableEvent event = new KeyUp(vector.x, vector.y, keycode);
		
		return world.emit(event, vector.x, vector.y);
	}
	
	
	public static class InputEvent extends ConsumeableEventAdapter{
		
	}
	
	
	public static class TouchDown extends InputEvent{
		public final float x, y;
		public final int button;
		
		
		public TouchDown(float x, float y, int button) {
			this.x = x;
			this.y = y;
			
			this.button = button;
		}
	}
	public static class TouchUp extends InputEvent{
		public final float x, y;
		public final int button;
		
		
		public TouchUp(float x, float y, int button) {
			this.x = x;
			this.y = y;
			
			this.button = button;
		}
	}
	public static class TouchDragged extends InputEvent{
		public final float x, y;

		
		public TouchDragged(float x, float y) {
			this.x = x;
			this.y = y;
		}
	}
	public static class MouseMoved extends InputEvent{
		public final float x, y;

		
		public MouseMoved(float x, float y) {
			this.x = x;
			this.y = y;
		}
	}
	public static class KeyTyped extends InputEvent{
		public final float x, y;
		public final char c;

		
		public KeyTyped(float x, float y, char c) {
			this.x = x;
			this.y = y;
			
			this.c = c;
		}
	}
	public static class KeyDown extends InputEvent{
		public final float x, y;
		public final int key;

		
		public KeyDown(float x, float y, int key) {
			this.x = x;
			this.y = y;
			
			this.key = key;
		}
	}
	public static class KeyUp extends InputEvent{
		public final float x, y;
		public final int key;

		
		public KeyUp(float x, float y, int key) {
			this.x = x;
			this.y = y;
			
			this.key = key;
		}
	}
}
