/**
 * package that contains internal classes that shouldn't be used outside of this project
 */
package com.felixcool98.gameworld.internal;