package com.felixcool98.gameworld.internal;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.felixcool98.gameworld.GameObject;
import com.felixcool98.step.StepMethod;
import com.felixcool98.step.utils.StepUtils;

public class GWOStepData {
	private List<StepMethod> stepMethods = new LinkedList<StepMethod>();
	
	
	@SuppressWarnings("rawtypes")
	public GWOStepData(Class<? extends GameObject> classs) {
		addAll(StepUtils.getStepMethods(classs));
	}
	
	
	public void add(StepMethod method) {
		stepMethods.add(method);
	}
	public void addAll(Collection<StepMethod> methods) {
		stepMethods.addAll(methods);
	}
	
	
	public List<StepMethod> getStepMethods() {
		return stepMethods;
	}
	
	
	/**
	 * @return true if there are any step methods registered else false
	 */
	public boolean canStep() {
		return !stepMethods.isEmpty();
	}
}
