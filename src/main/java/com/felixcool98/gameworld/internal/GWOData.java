package com.felixcool98.gameworld.internal;

import com.felixcool98.gameworld.GameObject;

public class GWOData {
	private GWOWorldData worldData;
	private GWOStepData stepData;
	private GWOFlags flags;
	
	
	@SuppressWarnings("rawtypes")
	public GWOData(String worldName, String[] interactionWorldNames, Class<? extends GameObject> classs) {
		worldData = new GWOWorldData(worldName, interactionWorldNames);
		stepData = new GWOStepData(classs);
		flags = new GWOFlags();
	}
	
	
	public GWOWorldData worldData() {
		return worldData;
	}
	public GWOStepData stepData() {
		return stepData;
	}
	public GWOFlags flags() {
		return flags;
	}
}
