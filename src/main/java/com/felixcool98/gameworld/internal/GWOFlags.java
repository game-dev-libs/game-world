package com.felixcool98.gameworld.internal;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GWOFlags {
	private Map<String, Boolean> flags = new HashMap<String, Boolean>();
	
	private List<FlagChanged> listeners = new LinkedList<GWOFlags.FlagChanged>();
	
	
	public void set(String flag, boolean value) {
		flags.put(flag, value);
		
		for(FlagChanged listener : listeners) {
			listener.flagChanged(flag, value);
		}
	}
	public void activate(String flag) {
		set(flag, true);
	}
	public void deactivate(String flag) {
		set(flag, false);
	}
	
	public boolean is(String flag) {
		if(!hasFlag(flag))
			return false;
		
		return flags.get(flag);
	}
	public boolean hasFlag(String name) {
		return flags.containsKey(name);
	}
	
	
	public void addListener(FlagChanged listener) {
		listeners.add(listener);
	}
	public void removeListener(FlagChanged listener) {
		listeners.remove(listener);
	}
	public void clearListener(FlagChanged listener) {
		listeners.clear();
	}
	
	
	public interface FlagChanged{
		public void flagChanged(String flag, boolean value);
	}
}
