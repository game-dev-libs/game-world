package com.felixcool98.gameworld.internal;

public class GWOWorldData {
	private String worldName;
	private String[] interactionWorldNames;
	
	
	public GWOWorldData(String worldName, String[] interactionWorldNames) {
		set(worldName, interactionWorldNames);
	}
	
	
	private void set(String worldName, String[] interactionWorldNames) {
		this.worldName = worldName;
		this.interactionWorldNames = interactionWorldNames;
	}
	
	
	public String getWorldName() {
		return worldName;
	}
	public String[] getInteractionWorldNames() {
		return interactionWorldNames;
	}
}
