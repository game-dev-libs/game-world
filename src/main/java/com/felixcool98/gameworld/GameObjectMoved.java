package com.felixcool98.gameworld;

public interface GameObjectMoved<A, B, W> {
	public void moved(GameObject<A, B, W> object);
}
