package com.felixcool98.gameworld;

import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * interface for all {@link GameObject}s that can be drawn
 * 
 * @author felixcool98
 */
public interface GameWorldDrawable {
	public void draw(Batch batch);
}
