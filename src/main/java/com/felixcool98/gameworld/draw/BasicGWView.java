package com.felixcool98.gameworld.draw;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.gameworld.GameObject;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.GameWorldDrawable;
import com.felixcool98.gameworld.ShapedGameObject;
import com.felixcool98.gameworld.object.GameWorldObject;

public class BasicGWView<A, B, W> extends GWView.GWViewImpl<A, B, W> {
	/**
	 * creates the view using the width to calculate the height via the aspect ratio
	 * 
	 * @param width viewPortWidth of the camera
	 * @param world
	 */
	public BasicGWView(float width, GameWorld world) {
		super(width, world);
	}
	/**
	 * @param width viewPortWidth of the camera
	 * @param height viewPortHeight of the camera
	 * @param world
	 */
	public BasicGWView(float width, float height, GameWorld world) {
		super(width, height, world);
	}
	public BasicGWView(OrthographicCamera camera, GameWorld world) {
		super(camera, world);
	}
	
	
	private List<GameWorldDrawable> getDrawables(Shape shape){
		List<GameWorldDrawable> drawables = new LinkedList<GameWorldDrawable>();
		
		for(GameObject<?, ?, ?> object : getWorld().getIntersections(shape)) {
			if(!(object instanceof GameWorldDrawable))
				continue;
			
			drawables.add((GameWorldDrawable) object);
		}
		
		drawables.sort(new Comparator<GameWorldDrawable>() {
			@Override
			public int compare(GameWorldDrawable arg0, GameWorldDrawable arg1) {
				return ((GameWorldObject) arg1).getDepth() - ((GameWorldObject) arg0).getDepth();
			}
		});
		
		return drawables;
	}

	
	@Override
	public void draw(Batch batch) {
		getCamera().update();
		
		batch.setProjectionMatrix(getCamera().combined);
		
		Color old = new Color(batch.getColor());
		
		for(GameWorldDrawable object : getDrawables(getBounds())) {
			object.draw(batch);
			batch.setColor(old);
		}
	}

	@Override
	public void debugDraw(ShapeRenderer renderer) {
		getCamera().update();
		
		renderer.setProjectionMatrix(getCamera().combined);
		
		for(GameObject<?, ?, ?> object : getWorld().getIntersections(getBounds())) {
			if(!(object instanceof ShapedGameObject))
				continue;
			
			ShapedGameObject shaped = (ShapedGameObject) object;
			
			renderer.rect(shaped.getShape().getAABB().getLeft(), shaped.getShape().getAABB().getBot(),
					shaped.getShape().getAABB().getWidth(), shaped.getShape().getAABB().getHeight());
		}
	}

	@Override
	public void drawDebugGrid(ShapeRenderer renderer) {
		getCamera().update();
		
		renderer.setProjectionMatrix(getCamera().combined);
		
		Rectangle bounds = getBounds();
	
		for(float x = (int) bounds.getLeft(); x <= bounds.getRight(); x++) {
			renderer.line(x, bounds.getBot(), x, bounds.getTop());
		}
		for(float y = (int) bounds.getBot(); y <= bounds.getTop(); y++) {
			renderer.line(bounds.getLeft(), y, bounds.getRight(), y);
		}
	}

	@Override
	public void drawDebugCamera(ShapeRenderer renderer) {
		Rectangle camera = getBounds();
		
		renderer.rect(camera.getX(), camera.getY(), camera.getWidth(), camera.getHeight());
		renderer.line(camera.getLeft(), camera.getBot(), camera.getRight(), camera.getTop());
		renderer.line(camera.getLeft(), camera.getTop(), camera.getRight(), camera.getBot());
	}

	@Override
	public void moved(GameObject<A, B, W> object) {
		if(object instanceof GameWorldObject) {
			GameWorldObject gwo = (GameWorldObject) object;
			
			getCamera().position.set(
					gwo.getShape().getAABB().getCenterX(),
					gwo.getShape().getAABB().getCenterY(),
					0);
		}
	}
}
