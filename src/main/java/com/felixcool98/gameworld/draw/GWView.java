package com.felixcool98.gameworld.draw;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.gameworld.GameObjectMoved;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gdxutility.GdxUtils;

public interface GWView<A, B, W> extends GameObjectMoved<A, B, W> {
	public void draw(Batch batch);
	public void debugDraw(ShapeRenderer renderer);
	/**
	 * draws a grid with a 1 unit gap between lines
	 * 
	 * @param renderer
	 */
	public void drawDebugGrid(ShapeRenderer renderer);
	public void drawDebugCamera(ShapeRenderer renderer);
	
	public void setCamera(OrthographicCamera camera);
	public OrthographicCamera getCamera();
	
	public Rectangle getBounds();
	
	
	public static abstract class GWViewImpl<A, B, W> implements GWView<A, B, W>{
		private OrthographicCamera camera;
		
		private GameWorld world;
		
		public GWViewImpl(float width, GameWorld world) {
			this(width, width*GdxUtils.getAspectRatio(), world);
		}
		public GWViewImpl(float width, float height, GameWorld world) {
			this(new OrthographicCamera(width, height), world);
		}
		public GWViewImpl(OrthographicCamera camera, GameWorld world) {
			this.camera = camera;
			this.world = world;
		}
		
		
		@Override
		public void setCamera(OrthographicCamera camera) {
			this.camera = camera;
		}
		@Override
		public OrthographicCamera getCamera() {
			return camera;
		}
		
		
		public GameWorld getWorld() {
			return world;
		}
		
		@Override
		public Rectangle getBounds() {
			OrthographicCamera camera = getCamera();
			
			return new Rectangle(camera.position.x-camera.viewportWidth/2f, camera.position.y-camera.viewportHeight/2f,
					camera.viewportWidth, camera.viewportHeight);
		}
	}
}
