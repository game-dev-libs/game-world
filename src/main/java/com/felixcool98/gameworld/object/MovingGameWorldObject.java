package com.felixcool98.gameworld.object;

import com.felixcool98.aabb.Shape;
import com.felixcool98.gameworld.events.CollisionEvent;
import com.felixcool98.gameworld.events.MovedEvent;
import com.felixcool98.step.Step;

/**
 * an {@link GameWorldObject} that has a x and y speed<br>
 * To allow collisions the object must handle the {@link CollisionEvent} method
 * 
 * @author felixcool98
 *
 */
public class MovingGameWorldObject extends GameWorldObject {
	private float xSpeed, ySpeed;
	
	
	@Deprecated
	public MovingGameWorldObject() {
		super(null);
	}
	public MovingGameWorldObject(Shape shape) {
		super(shape, "default");
	}
	public MovingGameWorldObject(Shape shape, String worldName) {
		super(shape, worldName, worldName);
	}
	public MovingGameWorldObject(Shape shape, String worldName, String...interactionWorldNames) {
		super(shape, worldName, interactionWorldNames);
	}
	
	
	@Step(group = "move", priority = 100)
	public void moveStep(float delta) {
		float xSpeed = getXSpeed()*delta;
		float ySpeed = getYSpeed()*delta;
		
		Shape copy = getShape().clone();
		
		move(xSpeed, ySpeed, getInteractionWorldNames());
		
		if(getShape().equals(copy))
			return;
		
		getGameWorld().emit(new MovedEvent(this, getShape().getAABB().getLeft()-copy.getAABB().getLeft(),
				getShape().getAABB().getBot()-copy.getAABB().getBot()), true);
	}
	
	
	public void setXSpeed(float speed) {
		this.xSpeed = speed;
	}
	public void setYSpeed(float speed) {
		this.ySpeed = speed;
	}
	public void setSpeed(float x, float y) {
		setXSpeed(x);
		setYSpeed(y);
	}
	
	public float getXSpeed() {
		return xSpeed;
	}
	public float getYSpeed() {
		return ySpeed;
	}
}
