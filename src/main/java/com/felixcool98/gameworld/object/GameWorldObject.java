package com.felixcool98.gameworld.object;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.felixcool98.aabb.Shape;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameObject;
import com.felixcool98.gameworld.ShapedGameObject;
import com.felixcool98.gameworld.events.BeforeCreationEvent;
import com.felixcool98.gameworld.events.CollisionEvent;
import com.felixcool98.gameworld.events.TouchEvent;
import com.felixcool98.utility.filter.AlwaysFilter;
import com.felixcool98.utility.filter.Filter;
import com.felixcool98.math.MathUtils;
import com.felixcool98.math.VectorMath;
import com.felixcool98.utility.values.AdvancedDirection;
import com.felixcool98.utility.values.Direction;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.adapter.World2DAdapter;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObject.World2DObjectChangeListener;
import com.felixcool98.worlds.objects.World2DObjectData;

public class GameWorldObject extends GameObject<World2DObjectData, World2DObject, World2D> implements ShapedGameObject {
	private World2DObjectChangeListener w2dObjectListener;
	private Shape shape;
	
	//movement
	
	private float stepSize = 0.1f;
	private List<GameObject<?, ?, ?>> receivedTouchEvents = new LinkedList<GameObject<?, ?, ?>>();
	
	
	@Deprecated
	public GameWorldObject() {
		this(null);
	}
	public GameWorldObject(Shape shape) {
		this(shape, "default");
	}
	public GameWorldObject(Shape shape, String worldName) {
		this(shape, worldName, worldName);
	}
	public GameWorldObject(Shape shape, String worldName, String...interactionWorldNames) {
		super(World2DAdapter.class, worldName, interactionWorldNames);
		
		this.shape = shape;
		
		w2dObjectListener = new World2DObjectChangeListener() {
			@Override
			public void changed(World2DObject object) {
				moved();
			}
		};
	}
	
	
	@EventListener
	public void createCreationData(BeforeCreationEvent event) {
		setWorldCreationData(new World2DObjectData(getShape()));
	}
	
	
	@Override
	public void setWorldObject(World2DObject object) {
		super.setWorldObject(object);
		
		if(getWorldObject() != null)
			getWorldObject().removeListener(w2dObjectListener);
		
		getWorldObject().addListener(w2dObjectListener);
		
		object.setUserObject(this);
		object.callListeners();
	}
	
	
	public void setStepSize(float stepSize) {
		this.stepSize = stepSize;
	}
	
	
	//======================================================================
	// checks with other gwos
	//======================================================================
	
	public float centerDistance(GameWorldObject other) {
		return VectorMath.vectorLength(getShape().getAABB().getCenterX(), getShape().getAABB().getCenterY(),
				other.getShape().getAABB().getCenterX(), other.getShape().getAABB().getCenterY());
	}
	public float aabbDistance(GameWorldObject other) {
		return getShape().getAABB().distanceTo(other.getShape().getAABB());
	}
	
	
	//======================================================================
	// user methods
	//======================================================================
	
	public void update() {
		if(getWorldObject() == null)
			return;
		
		getWorldObject().setShape(shape);
	}
	
	
	//intersections
	
	public List<GameObject<?, ?, ?>> getIntersections(float x, float y) {
		return getIntersections(x, y, getInteractionWorldNames());
	}
	public List<GameObject<?, ?, ?>> getIntersections(float x, float y, String...worldNames) {
		return getGameWorld().getIntersections(x, y, worldNames);
	}
	public <T> List<T> getIntersections(float x, float y, Class<T> classs, String...worldNames) {
		List<T> intersections = getGameWorld().getIntersections(x, y, classs, worldNames);
		
		intersections.remove(this);
		
		return intersections;
	}
	
	
	public List<GameObject<?, ?, ?>> getIntersectionsAllLayers(float x, float y) {
		return getGameWorld().getIntersections(x, y);
	}
	
	public List<GameObject<?, ?, ?>> getIntersections() {
		return getIntersections(shape, getInteractionWorldNames());
	}
	public List<GameObject<?, ?, ?>> getIntersections(Shape shape) {
		return getIntersections(shape, getInteractionWorldNames());
	}
	public List<GameObject<?, ?, ?>> getIntersections(String...worldNames) {
		return getIntersections(shape, worldNames);
	}
	public List<GameObject<?, ?, ?>> getIntersections(Shape shape, String...worldNames) {
		List<GameObject<?, ?, ?>> intersections = getGameWorld().getIntersections(shape, worldNames);
		
		intersections.remove(this);
		
		return intersections;
	}
	
	public List<GameObject<?, ?, ?>> getIntersectionsAllLayers(Shape shape) {
		List<GameObject<?, ?, ?>> intersections = getGameWorld().getIntersections(shape);
		
		intersections.remove(this);
		
		return intersections;
	}
	
	
	@Override
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	@Override
	public Shape getShape() {
		return shape;
	}
	
	
	public AdvancedDirection getSurrounding() {
		return getSurrounding(0.1f, new AlwaysFilter<>());
	}
	public AdvancedDirection getSurrounding(float margin, Filter<GameObject<?, ?, ?>> filter) {
		return getGameWorld().getSurrounding(shape, margin, filter, getInteractionWorldNames());
	}
	
	
	public List<GameObject<?, ?, ?>>[][] getSurroundingAsArray(){
		return getSurroundingAsArray(0.1f, new AlwaysFilter<GameObject<?, ?, ?>>());
	}
	public List<GameObject<?, ?, ?>>[][] getSurroundingAsArray(float margin, Filter<GameObject<?, ?, ?>> filter){
		return getGameWorld().getSurroundingAsArray(shape, margin, filter, getInteractionWorldNames());
	}
	
	
	//======================================================================
	// moving
	//======================================================================
	
	/**
	 * moves the object<br>
	 * if the object collides with any other object in the given worlds it will fire a {@link CollisionEvent}<br>
	 * If this event gets consumed the object will stop at the given object trying to get as close as possible to the other one<br>
	 * If the object is already inside of an object a {@link CollisionEvent} will be fired and if consumed the object will not move at all <br>
	 * the object will take multiple steps, the size of each step can be changed with {@link GameWorldObject#setStepSize(float)}
	 * 
	 * @param x
	 * @param y
	 * @param collisionWorldNames
	 */
	public void move(float x, float y, String... collisionWorldNames) {
		if(MathUtils.allZero(x, y)) 
			return;
		
		for(GameObject<?, ?, ?> object : getIntersections()) {
			if(collision(object))
				return;
		}
		
		receivedTouchEvents.clear();
		
		Vector2 vec = new Vector2(x, y);
		
		int steps = (int)(vec.len() / stepSize);
		
		vec.setLength(stepSize);
		
		Vector2 rest = new Vector2(x-vec.x*(float)steps, y-vec.y*(float)steps);
		
		Shape copy = getShape().clone();
		
		for(float i = 0; i < steps; i++) {
			if(singleStep(copy, vec.x, vec.y, collisionWorldNames))
				break;
		}
		
		if(rest.x != 0 || rest.y != 0) {
			singleStep(copy, rest.x, rest.y, collisionWorldNames);
		}
		
		getShape().move(copy.getAABB().getLeft() - getShape().getAABB().getLeft(),
				copy.getAABB().getBot() - getShape().getAABB().getBot());
	}
	private boolean singleStep(Shape shape, float x, float y, String...collisionWorldNames) {
		shape.move(x, y);
		
		List<GameObject<?, ?, ?>> intersections = getIntersections(shape, collisionWorldNames);
		
		boolean collision = false;
		
		for(GameObject<?, ?, ?> object : intersections) {
			if(collision(object)) {
				if(!receivedTouchEvents.contains(object)) {
					object.emit(new TouchEvent(this, object));
					
					receivedTouchEvents.add(object);
				}
				
				collision = true;
				
				break;
			}
		}
		
		if(collision) {
			shape.move(-x, -y);
			
			Direction dir = tryTouch(shape, x, y, collisionWorldNames);
			
			if(dir.isSingular()) {
				if(dir.isHorizontal())
					x = 0;
				else
					y = 0;
				
				shape.move(x, y);
			}else if(dir.get() == Direction.NONE){
				shape.move(x, y);
			}else {
				return true;
			}
		}
		
		return false;
	}
	private boolean collision(GameObject<?, ?, ?> object) {
		CollisionEvent event = new CollisionEvent(this, object);
		
		getManager().emit(event);
		
		return event.isConsumed();
	}
	/**
	 * moves the shape directly in the given direction<br>
	 * if any other object intersect at the new position the object will be moved back so that they touch each other
	 * 
	 * @param shape
	 * @param x
	 * @param y
	 * @return
	 */
	public Direction tryTouch(Shape shape, float x, float y, String...worldNames) {
		Direction dir = new Direction();
		
		Shape hor = shape.clone();
		
		hor.move(x, 0);
		
		for(GameObject<?, ?, ?> object : getIntersections(hor, worldNames)) {
			if(!collision(object))
				continue;
			
			if(!receivedTouchEvents.contains(object)) {
				object.emit(new TouchEvent(this, object));
				
				receivedTouchEvents.add(object);
			}
			
			hor.move(-x, 0);
			
			if(!(object instanceof ShapedGameObject))
				break;
			
			ShapedGameObject shaped = (ShapedGameObject) object;
			
			if(x < 0) {//moves left
				hor.setLeft(shaped.getShape().getAABB().getRight());
				dir.left();
			}else {//moves right
				hor.setRight(shaped.getShape().getAABB().getLeft());
				dir.right();
			}
			
			break;
		}
		
		Shape vert = shape.clone();
		
		vert.move(0, y);
		
		for(GameObject<?, ?, ?> object : getIntersections(vert, worldNames)) {
			if(!collision(object))
				continue;
			
			if(!receivedTouchEvents.contains(object)) {
				object.emit(new TouchEvent(this, object));
				
				receivedTouchEvents.add(object);
			}
			
			vert.move(0, -y);
			
			if(!(object instanceof ShapedGameObject))
				break;
			
			ShapedGameObject shaped = (ShapedGameObject) object;
			
			if(y < 0) {//moves down
				vert.setBot(shaped.getShape().getAABB().getTop());
				dir.bot();
			}else {//moves up
				vert.setTop(shaped.getShape().getAABB().getBot());
				dir.top();
			}
			
			break;
		}
		
		if(dir.isHorizontal()) {
			shape.move(hor.getAABB().getLeft() - shape.getAABB().getLeft(), 0);
		}
		if(dir.isVertical()) {
			shape.move(0, vert.getAABB().getBot() - shape.getAABB().getBot());
		}
		
		return dir;
	}
}
