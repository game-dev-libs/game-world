package com.felixcool98.gameworld;

import java.util.Comparator;

/**
 * comparator for sorting GWOs by their depth typically from lowest to highest depth
 * 
 * @author felixcool98
 */
public class DepthComparator implements Comparator<GameObject<?, ?, ?>> {
	@Override
	public int compare(GameObject<?, ?, ?> arg0, GameObject<?, ?, ?> arg1) {
		return arg1.getDepth() - arg0.getDepth();
	}
}
