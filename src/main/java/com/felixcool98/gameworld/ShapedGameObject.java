package com.felixcool98.gameworld;

import com.felixcool98.aabb.Shape;

/**
 * interface for all {@link GameObject}s that have a distinct shape
 * 
 * @author felixcool98
 */
public interface ShapedGameObject {
	public void setShape(Shape shape);
	
	public Shape getShape();
}
