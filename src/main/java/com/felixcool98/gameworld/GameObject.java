package com.felixcool98.gameworld;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.events.EventManager;
import com.felixcool98.gameworld.internal.GWOData;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.step.StepMethod;
import com.felixcool98.worlds.WorldAdapter;

public class GameObject<A, B, W> {
	private GWOData data;
	
	
	private GameWorld world;
	
	private EventManager manager = new EventManager();
	
	private A worldCreationData;
	private B worldObject;
	private Class<? extends WorldAdapter<A, B, W>> worldClass;
	
	private List<GameObjectMoved<A, B, W>> listeners = new LinkedList<GameObjectMoved<A, B, W>>();
	
	private int depth;
	
	
	public GameObject(Class<? extends WorldAdapter<A, B, W>> worldClass) {
		this(worldClass, "default");
	}
	public GameObject(Class<? extends WorldAdapter<A, B, W>> worldClass, String worldName) {
		this(worldClass, worldName, worldName);
	}
	public GameObject(Class<? extends WorldAdapter<A, B, W>> worldClass, String worldName, String...interactionWorldNames) {
		data = new GWOData(worldName, interactionWorldNames, getClass());
		
		manager.register(this);
		
		this.worldClass = worldClass;
	}
	
	
	public GameWorld getGameWorld() {
		return world;
	}
	public void setWorld(GameWorld world) {
		this.world = world;
	}
	
	
	public void setWorldCreationData(A worldCreationData) {
		this.worldCreationData = worldCreationData;
	}
	public A getWorldCreationData() {
		return worldCreationData;
	}
	public void setWorldObject(B object) {
		this.worldObject = object;
	}
	public B getWorldObject() {
		return worldObject;
	}
	public Class<? extends WorldAdapter<A, B, W>> getWorldClass() {
		return worldClass;
	}
	
	
	public EventManager getManager() {
		return manager;
	}
	
	public void emit(Object event) {
		getManager().emit(event);
	}
	
	
	public GWOData data() {
		return data;
	}
	
	public String[] getInteractionWorldNames() {
		return data().worldData().getInteractionWorldNames();
	}
	public String getWorldName() {
		return data().worldData().getWorldName();
	}
	
	
	public final void destroy() {
		world.destroy(this);
	}
	public void create(GameWorldObject object) {
		world.create(object);
	}
	
	
	public void update() {}
	public void moved() {
		for(GameObjectMoved<A, B, W> listener : listeners)
			listener.moved(this);
	}
	
	
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public int getDepth() {
		return depth;
	}
	
	
	public void addListener(GameObjectMoved<A, B, W> listener) {
		listeners.add(listener);
	}
	public void removeListener(GameObjectMoved<A, B, W> listener) {
		listeners.remove(listener);
	}
	
	
	public void setSleeping(boolean value) {
		data().flags().set("sleeping", value);
	}
	
	public void sleep() {
		setSleeping(true);
	}
	public void wakeUp() {
		setSleeping(false);
	}
	
	public boolean isSleeping() {
		return data().flags().is("sleeping");
	}
	
	
	public void addStepMethod(StepMethod method) {
		if(!method.valid(this))
			throw new IllegalArgumentException(method+" is not valid for "+this);
		
		data().stepData().add(method);
	}
}

