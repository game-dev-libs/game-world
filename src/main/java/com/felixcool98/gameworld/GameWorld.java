package com.felixcool98.gameworld;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.utils.Disposable;
import com.felixcool98.aabb.Shape;
import com.felixcool98.events.ConsumableEvent;
import com.felixcool98.events.EventManager;
import com.felixcool98.gameworld.events.BeforeCreationEvent;
import com.felixcool98.gameworld.events.CreationEvent;
import com.felixcool98.gameworld.events.DestroyEvent;
import com.felixcool98.gameworld.events.GameWorldEvent;
import com.felixcool98.gameworld.exceptions.MissingWorldException;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.step.StepMethod;
import com.felixcool98.step.WorldStep;
import com.felixcool98.step.utils.Tuple2;
import com.felixcool98.utility.filter.Filter;
import com.felixcool98.utility.values.AdvancedDirection;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.WorldAdapter;
import com.felixcool98.worlds.WorldManager;
import com.felixcool98.worlds.adapter.World2DAdapter;
import com.felixcool98.worlds.bruteforce.BruteForceWorld;
import com.felixcool98.worlds.objects.World2DObject;

public class GameWorld {
	private WorldStep step = new WorldStep();
	private WorldManager worlds = new WorldManager();
	
	private List<GameObject<?, ?, ?>> objects = new LinkedList<>();
	
	//hashsets so objects can't be destroyed or created multiple times
	private Set<GameObject<?, ?, ?>> create = new HashSet<>();
	private Set<GameObject<?, ? ,?>> destroy = new HashSet<>();
	
	private EventManager manager = new EventManager();
	
	
	public GameWorld() {
		this(new World2DAdapter(new BruteForceWorld()));
	}
	public GameWorld(WorldAdapter<?, ?, ?> world) {
		add("default", world);
		
		step.setAfterStep((object)->{
			if(object instanceof GameObject<?, ?, ?>)
				((GameObject<?, ?, ?>) object).update();
		});
	}
	
	
	//======================================================================
	// World2D
	//======================================================================
	
	public void add(String name, World2D world) {
		worlds.add(name, new World2DAdapter(world));
	}
	public void add(String name, WorldAdapter<?, ?, ?> world) {
		worlds.add(name, world);
	}
	
	public void remove(String name) {
		worlds.remove(name);
	}
	
	
	//======================================================================
	// adding removing
	//======================================================================
	
	/**
	 * forces the world to destroy and create all objects in the queue
	 */
	public void forceUpdate() {
		internalDestroyAll();
		internalCreateAll();
	}
	
	public void create(GameWorldObject object) {
		create.add(object);
	}
	
	private void internalCreateAll() {
		for(GameObject<?, ?, ?> object : create) {
			internalCreate(object);
		}
		
		create.clear();
	}
	private <A, B, W> void internalCreate(GameObject<A, B, W> object) {
		BeforeCreationEvent event = new BeforeCreationEvent(object);
		
		emit(event, true);
		
		if(event.isConsumed())
			return;
		
		if(!worlds.contains(object.getWorldName())) 
			throw new MissingWorldException(object.getWorldName()+" couldn't be found, but is required for "+object);
		
		objects.add(object);
		
		A a = object.getWorldCreationData();
		B b = worlds.get(object.getWorldName(), object.getWorldClass()).create(a);
		
		object.setWorld(this);
		object.setWorldObject(b);
		
		emit(new CreationEvent(object), true);
	}
	
	
	/**
	 * will attempt to destroy the object in the next step<br>
	 * this will set sleeping to true, reusing the object needs to manually set sleeping to false if stepping is needed
	 * 
	 * @param object
	 */
	public void destroy(GameObject<?, ?, ?> object) {
		object.setSleeping(true);
		destroy.add(object);
	}
	
	private void internalDestroyAll() {
		for(GameObject<?, ?, ?> object : destroy) {
			internalDestroy(object);
		}
		
		destroy.clear();
	}
	private <A, B, W> void internalDestroy(GameObject<A, B, W> object) {
		objects.remove(object);
		
		if(object.getWorldObject() instanceof World2DObject) {
			((World2DObject) object.getWorldObject()).destroy();
		}else if(object.getWorldObject() instanceof Disposable) {
			((Disposable) object.getWorldObject()).dispose();
		}
		
		emit(new DestroyEvent(object), true);
	}
	
	
	//======================================================================
	// events
	//======================================================================
	
	public void emit(GameWorldEvent event, boolean emitToSource) {
		manager.emit(event);
		
		if(emitToSource)
			event.source.emit(event);
	}
	
	public void register(Object object) {
		manager.register(object);
	}
	public void unregister(Object object) {
		manager.unregister(object);
	}
	
	/**
	 * emit the given event to all objects on the given coordinates<br>
	 * the underlying objects will be sorted by there depth, from lowest to highest
	 * 
	 * @param event
	 * @param x
	 * @param y
	 * @return true if the event got consumed otherwise false
	 */
	public boolean emit(ConsumableEvent event, float x, float y) {
		List<GameObject<?, ?, ?>> objects = getIntersections(x, y);
		
		objects.sort(new DepthComparator());
		
		java.util.Collections.reverse(objects);
		
		for(GameObject<?, ?, ?> object : objects) {
			object.emit(event);
			
			if(event.isConsumed())
				return true;
		}
		
		return false;
	}
	
	
	//======================================================================
	// step
	//======================================================================
	
	/**
	 * steps trough the world<br<
	 * destroys all destroyed objects<br>
	 * creates all new objects<br>
	 * steps trough every objects {@link StepMethod}s
	 * 
	 * @param delta time between this and the last step
	 */
	public void step(float delta) {
		forceUpdate();
		
		List<Tuple2<Object, StepMethod>> objects = new LinkedList<Tuple2<Object,StepMethod>>();
		
		for(GameObject<?, ?, ?> object : getSteppables()) {
			for(StepMethod method : object.data().stepData().getStepMethods()) {
				objects.add(new Tuple2<Object, StepMethod>(object, method));
			}
		}
		
		step.step(objects, delta);
	}
	
	
	private List<GameObject<?, ?, ?>> getSteppables(){
		List<GameObject<?, ?, ?>> steppables = new LinkedList<>();
		
		for(GameObject<?, ?, ?> object : objects) {
			if(object.isSleeping())//if the object is sleeping skip it
				continue;
			if(!object.data().stepData().canStep())//if there are no stepmethods skip the object
				continue;
			
			steppables.add(object);
		}
		
		return steppables;
	}
	
	
	//======================================================================
	// intersections
	//======================================================================
	
	@SuppressWarnings("unchecked")
	private List<GameObject<?, ?, ?>> convert(List<Object> objects){
		return convertGO(objects, GameObject.class);
	}
	@SuppressWarnings("unchecked")
	private <A, B, C, T extends GameObject<A, B, C>> List<T> convertGO(List<Object> objects, Class<T> classs){
		List<T> intersections = new LinkedList<>();
		
		for(Object object : objects) {
			if(!classs.isInstance(object))
				continue;
			
			intersections.add((T) object);
		}
		
		intersections.sort(new DepthComparator());
		
		return intersections;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private <T> List<T> convertAny(List<Object> objects, Class<T> classs){
		List<T> intersections = new LinkedList<>();
		
		for(Object object : objects) {
			if(!classs.isInstance(object))
				continue;
			
			intersections.add((T) object);
		}
		
		DepthComparator comparator = new DepthComparator();
		
		intersections.sort((obj1, obj2)->{
			if(obj1 instanceof GameObject && obj2 instanceof GameObject) {
				return comparator.compare((GameObject)obj1, (GameObject)obj2);
			}else if(obj1 instanceof GameObject)
				return -1;
			else if(obj2 instanceof GameObject)
				return 1;
			
			return 0;
		});
		
		return intersections;
	}
	
	
	public List<GameObject<?, ?, ?>> getIntersections(float x, float y) {
		return convert(worlds.getIntersections(x, y));
	}
	public <T> List<T> getIntersections(float x, float y, Class<T> classs) {
		return convertAny(worlds.getIntersections(x, y), classs);
	}
	
	public List<GameObject<?, ?, ?>> getIntersections(float x, float y, String... worldNames) {
		return convert(worlds.getIntersections(x, y, worldNames));
	}
	public <T> List<T> getIntersections(float x, float y, Class<T> classs, String... worldNames) {
		return convertAny(worlds.getIntersections(x, y, worldNames), classs);
	}
	
	
	public List<GameObject<?, ?, ?>> getIntersections(Shape shape) {
		return convert(worlds.getIntersections(shape));
	}
	public <T> List<T> getIntersections(Shape shape, Class<T> classs) {
		return convertAny(worlds.getIntersections(shape), classs);
	}
	
	public  List<GameObject<?, ?, ?>> getIntersections(Shape shape, String... worldNames) {
		return convert(worlds.getIntersections(shape, worldNames));
	}
	public <T> List<T> getIntersections(Shape shape, Class<T> classs, String... worldNames) {
		return convertAny(worlds.getIntersections(shape, worldNames), classs);
	}
	
	
	public AdvancedDirection getSurrounding(Shape shape, float margin, Filter<GameObject<?, ?, ?>> filter, String...worlds) {
		List<GameObject<?, ?, ?>>[][] array = getSurroundingAsArray(shape, margin, filter, worlds);
		
		AdvancedDirection direction = new AdvancedDirection();
		
		if(!array[0][2].isEmpty())
			direction.topLeft();
		if(!array[1][2].isEmpty())
			direction.top();
		if(!array[2][2].isEmpty())
			direction.topRight();
		
		if(!array[0][1].isEmpty())
			direction.left();
		if(!array[2][1].isEmpty())
			direction.right();
		
		if(!array[0][0].isEmpty())
			direction.botLeft();
		if(!array[1][0].isEmpty())
			direction.bot();
		if(!array[2][0].isEmpty())
			direction.botRight();
		
		return direction;
	}
	/**
	 * 
	 * @param shape
	 * @param margin
	 * @param filter
	 * @param worlds
	 * @return two dim array with lists of objects surrounding the given shape<br>
	 * 0 1 left
	 * 2 1 right
	 * 1 2 top
	 * 1 0 bot
	 * 0 2 top left
	 * 2 2 top right
	 * 0 0 bot left
	 * 0 2 bot right
	 */
	public List<GameObject<?, ?, ?>>[][] getSurroundingAsArray(Shape shape, float margin, Filter<GameObject<?, ?, ?>> filter, String...worlds){
		@SuppressWarnings("unchecked")
		List<GameObject<?, ?, ?>>[][] array = new List[3][3];
		List<Object>[][] result = this.worlds.getSurroundingAsArray(shape, margin, worlds);
		
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				array[i][j] = filter.filter(convert(result[i][j]));
			}
		}
		
		return array;
	}
	
	
	//===============================================================
	// stats
	//===============================================================
	
	public GameWorldStats getStats() {
		GameWorldStats stats = new GameWorldStats();
		
		for(String world : worlds.names()) {
			World2D world2D = worlds.get(world, World2DAdapter.class).getWorld();
			
			if(world2D == null)
				continue;
			
			stats.add(world, world2D.size());
		}
		
		return stats;
	}
}
