package com.felixcool98.gameworld.events;

import com.felixcool98.gameworld.GameObject;

/**
 * fired once a collision was handled
 * 
 * @author felixcool98
 *
 */
public class TouchEvent extends GameWorldEvent {
	public final GameObject<?, ?, ?> other;
	
	
	public TouchEvent(GameObject<?, ?, ?> source, GameObject<?, ?, ?> other) {
		super(source);
		
		this.other = other;
	}

}
