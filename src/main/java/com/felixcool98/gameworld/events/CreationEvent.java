package com.felixcool98.gameworld.events;

import com.felixcool98.gameworld.GameObject;

public class CreationEvent extends GameWorldEvent {
	public CreationEvent(GameObject<?, ?, ?> source) {
		super(source);
	}
}
