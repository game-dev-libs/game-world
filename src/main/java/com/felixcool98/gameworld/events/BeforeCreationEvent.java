package com.felixcool98.gameworld.events;

import com.felixcool98.gameworld.GameObject;

/**
 * will be called before the object is added to the world<br>
 * if the event gets consumed the creation will be stopped
 * 
 * @author felixcool98
 *
 */
public class BeforeCreationEvent extends GameWorldEvent {
	public BeforeCreationEvent(GameObject<?, ?, ?> source) {
		super(source);
	}
}
