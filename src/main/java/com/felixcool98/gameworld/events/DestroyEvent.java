package com.felixcool98.gameworld.events;

import com.felixcool98.gameworld.GameObject;

public class DestroyEvent extends GameWorldEvent {
	public DestroyEvent(GameObject<?, ?, ?> source) {
		super(source);
	}
}
