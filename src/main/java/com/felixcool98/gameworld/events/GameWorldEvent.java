package com.felixcool98.gameworld.events;

import com.felixcool98.events.ConsumableEvent.ConsumeableEventAdapter;
import com.felixcool98.gameworld.GameObject;

public class GameWorldEvent extends ConsumeableEventAdapter {
	public final GameObject<?, ?, ?> source;
	
	
	public GameWorldEvent(GameObject<?, ?, ?> source) {
		this.source = source;
	}
}
