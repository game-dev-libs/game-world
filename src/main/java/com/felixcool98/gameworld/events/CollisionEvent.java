package com.felixcool98.gameworld.events;

import com.felixcool98.gameworld.GameObject;

/**
 * will be created in the "move" group step with the priority 100 by the MovingGameWorldObject class
 * 
 * @author felixcool98
 *
 */
public class CollisionEvent extends GameWorldEvent {
	public final GameObject<?, ?, ?> other;
	
	
	public CollisionEvent(GameObject<?, ?, ?> source, GameObject<?, ?, ?> other) {
		super(source);
		
		this.other = other;
	}
}
