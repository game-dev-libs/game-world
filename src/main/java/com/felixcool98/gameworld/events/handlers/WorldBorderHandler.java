package com.felixcool98.gameworld.events.handlers;

import com.felixcool98.aabb.AABB;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameObject;
import com.felixcool98.gameworld.events.BeforeCreationEvent;
import com.felixcool98.gameworld.events.MovedEvent;
import com.felixcool98.gameworld.object.GameWorldObject;

public class WorldBorderHandler {
	private Rectangle borders;
	
	
	public WorldBorderHandler(Rectangle borders) {
		this.borders = borders;
	}
	
	
	@EventListener
	public void moved(MovedEvent event) {
		GameObject<?, ?, ?> object = event.source;
		
		if(object instanceof GameWorldObject) {
			GameWorldObject gwo = (GameWorldObject) object;
			
			AABB aabb = gwo.getShape().getAABB();
			
			if(borders.getAABB().contains(aabb))
				return;
			
			moveInside(gwo);
		}
		
		
	}
	@EventListener
	public void created(BeforeCreationEvent event) {
		GameObject<?, ?, ?> object = event.source;
		
		if(object instanceof GameWorldObject) {
			GameWorldObject gwo = (GameWorldObject) object;
			
			AABB aabb = gwo.getShape().getAABB();
			
			if(borders.getAABB().contains(aabb))
				return;
			
			moveInside(gwo);
		}
	}
	
	
	private void moveInside(GameWorldObject object) {
		AABB aabb = object.getShape().getAABB();
		
		if(aabb.getLeft() < borders.getLeft())
			object.getShape().setLeft(borders.getLeft());
		
		if(aabb.getBot() < borders.getBot())
			object.getShape().setBot(borders.getBot());
		
		if(aabb.getTop() > borders.getTop())
			object.getShape().setTop(borders.getTop());
		
		if(aabb.getRight() > borders.getRight())
			object.getShape().setRight(borders.getRight());
	}
}
