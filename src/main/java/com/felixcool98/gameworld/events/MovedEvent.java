package com.felixcool98.gameworld.events;

import com.felixcool98.gameworld.object.GameWorldObject;

/**
 * will be sent once an MoveableGWO moves
 * 
 * @author felixcool98
 */
public class MovedEvent extends GameWorldEvent {
	public final float x;
	public final float y;
	
	
	public MovedEvent(GameWorldObject source, float x, float y) {
		super(source);
		
		this.x = x;
		this.y = y;
	}
}
