package com.felixcool98.gameworld;

import java.util.LinkedList;
import java.util.List;

public class GameWorldStats {
	private List<WorldStats> worlds = new LinkedList<GameWorldStats.WorldStats>();
	
	private int totalSize = 0;
	
	
	public void add(String name, int size) {
		worlds.add(new WorldStats(name, size));
		
		totalSize += size;
	}
	
	public List<WorldStats> getWorlds() {
		return worlds;
	}
	public int getTotalSize() {
		return totalSize;
	}
	
	
	public static class WorldStats{
		public final String name;
		public final int size;
		
		
		public WorldStats(String name, int size) {
			this.name = name;
			this.size = size;
		}
	}
}
