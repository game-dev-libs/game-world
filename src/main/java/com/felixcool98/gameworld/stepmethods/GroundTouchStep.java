package com.felixcool98.gameworld.stepmethods;

import java.util.HashSet;
import java.util.List;

import com.felixcool98.aabb.AABB;
import com.felixcool98.aabb.shapes.RelativeLine;
import com.felixcool98.gameworld.GameObject;
import com.felixcool98.gameworld.object.GameWorldObject;
import com.felixcool98.step.StepMethod;

public class GroundTouchStep implements StepMethod {
	private boolean sendOnce = false;
	private HashSet<GameWorldObject> set;
	
	
	public GroundTouchStep() {
		this(false);
	}
	public GroundTouchStep(boolean sendOnce) {
		this.sendOnce = sendOnce;
		
		if(sendOnce)
			set = new HashSet<GameWorldObject>();
	}
	
	
	
	@Override
	public void step(Object object, float delta) {
		step((GameWorldObject) object, delta);
	}
	private void step(GameWorldObject object, float delta) {
		if(!sendOnce) {
			if(onGround(object))
				object.emit(new TouchedGround());
		}else 
			sendOnce(object);
	}
	private void sendOnce(GameWorldObject object) {
		if(onGround(object)) {
			if(set.contains(object))
				return;
			
			object.emit(new TouchedGround());
			
			set.add(object);
		}else {
			if(set.contains(object))
				set.remove(object);
		}
	}
	
	
	public boolean onGround(GameWorldObject object) {
		return !groundIntersections(object).isEmpty();
	}
	public List<GameObject<?, ?, ?>> groundIntersections(GameWorldObject object){
		AABB aabb = object.getShape().getAABB();
		
		return object.getIntersections(new RelativeLine(aabb.getLeft(), aabb.getBot(),
				aabb.getWidth(), 0));
	}
	
	
	@Override
	public boolean valid(Object object) {
		return object instanceof GameWorldObject;
	}
}
