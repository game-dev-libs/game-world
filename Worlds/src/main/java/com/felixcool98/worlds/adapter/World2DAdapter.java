package com.felixcool98.worlds.adapter;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.aabb.AABB;
import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.worlds.Intersections;
import com.felixcool98.worlds.PointIntersections;
import com.felixcool98.worlds.ShapeIntersections;
import com.felixcool98.worlds.Surrounding;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.WorldAdapter;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObjectData;

public class World2DAdapter implements WorldAdapter<World2DObjectData, World2DObject, World2D>, PointIntersections, ShapeIntersections, Surrounding {
	private World2D world;
	
	
	public World2DAdapter(World2D world) {
		this.world = world;
	}


	@Override
	public World2DObject create(World2DObjectData creationData) {
		return world.create(creationData);
	}
	

	@Override
	public World2D getWorld() {
		return world;
	}
	
	
	private List<Object> convert(Intersections intersections){
		List<Object> objects = new LinkedList<Object>();
		
		for(World2DObject object : intersections) {
			Object usertObject = object.getUserObject();//user object should be a GO or GWO
			
			if(usertObject == null)
				continue;
			
			objects.add(usertObject);
		}
		
		intersections.free();
		
		return objects;
	}
	
	
	@Override
	public List<Object> getIntersections(float x, float y) {
		return convert(world.getIntersections(x, y));
	}
	@Override
	public List<Object> getIntersections(Shape shape) {
		return convert(world.getIntersections(shape));
	}
	@Override
	public void getSurroundingAsArray(Shape shape, float margin, List<Object>[][] array) {
		AABB aabb = shape.getAABB();
		
		Rectangle rect = new Rectangle(0, 0);
		
		//left
		rect.setSize(margin, aabb.getHeight());
		rect.setBot(aabb.getBot());
		rect.setRight(aabb.getLeft());
		
		array[0][1] = getIntersections(rect);
		
		//right
		rect.setLeft(aabb.getRight());
		
		array[2][1] = getIntersections(rect);
		
		//top
		rect.setSize(aabb.getWidth(), margin);
		rect.setBot(aabb.getTop());
		rect.setLeft(aabb.getLeft());
		
		array[1][2] = getIntersections(rect);
		
		//bot
		rect.setTop(aabb.getBot());
		
		array[1][0] = getIntersections(rect);
		
		//top left
		rect.setSize(margin, margin);
		rect.setBot(aabb.getTop());
		rect.setRight(aabb.getLeft());
		
		array[0][2] = getIntersections(rect);
		
		//top right
		rect.setLeft(aabb.getRight());
		
		array[2][2] = getIntersections(rect);
		
		//bot left
		rect.setRight(aabb.getLeft());
		rect.setTop(aabb.getBot());
		
		array[0][0] = getIntersections(rect);
		
		//bot right
		rect.setLeft(aabb.getRight());
		
		array[2][0] = getIntersections(rect);
	}
}
