package com.felixcool98.worlds;

public interface WorldAdapter<A, B, W> {
	public B create(A creationData);
	
	public W getWorld();
}
