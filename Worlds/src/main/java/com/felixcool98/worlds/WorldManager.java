package com.felixcool98.worlds;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.felixcool98.aabb.Shape;

public class WorldManager {
	private Map<String, WorldAdapter<?, ?, ?>> worlds = new HashMap<String, WorldAdapter<?, ?, ?>>();
	
	
	public void add(String name, WorldAdapter<?, ?, ?> world) {
		worlds.put(name, world);
	}
	
	public void remove(String name) {
		worlds.remove(name);
	}
	
	public boolean contains(String name) {
		return worlds.containsKey(name);
	}
	
	
	public Set<String> names(){
		return worlds.keySet();
	}
	
	
	@SuppressWarnings("unchecked")
	public <A, B, W, T extends WorldAdapter<A, B, W>> T get(String name, Class<T> classs) {
		WorldAdapter<A, B, W> world = (WorldAdapter<A, B, W>) worlds.get(name);
		
		if(!classs.isInstance(world))
			return null;
		
		return (T) world;
	}
	
	
	public List<Object> getIntersections(float x, float y) {
		return getIntersections(x, y, names().toArray(new String[0]));
	}
	public List<Object> getIntersections(float x, float y, String... worldNames) {
		List<Object> out = new LinkedList<>();
		
		for(String world : worldNames) {
			@SuppressWarnings("unchecked")
			WorldAdapter<?, ?, ?> adapter = get(world, WorldAdapter.class);
			
			if(adapter == null)
				continue;
			if(!(adapter instanceof PointIntersections))
				continue;
			
			PointIntersections intersections = (PointIntersections) adapter;
			
			out.addAll(intersections.getIntersections(x, y));
		}
		
		return out;
	}
	
	public List<Object> getIntersections(Shape shape) {
		return getIntersections(shape, names().toArray(new String[0]));
	}
	public List<Object> getIntersections(Shape shape, String... worldNames) {
		List<Object> out = new LinkedList<>();
		
		for(String world : worldNames) {
			@SuppressWarnings("unchecked")
			WorldAdapter<?, ?, ?> adapter = get(world, WorldAdapter.class);
			
			if(adapter == null)
				continue;
			if(!(adapter instanceof ShapeIntersections))
				continue;
			
			ShapeIntersections intersections = (ShapeIntersections) adapter;
			
			out.addAll(intersections.getIntersections(shape));
		}
		
		return out;
	}
	
	/**
	 * 
	 * @param shape
	 * @param margin
	 * @param filter
	 * @return two dim array with lists of objects surrounding the given shape<br>
	 * 0 1 left
	 * 2 1 right
	 * 1 2 top
	 * 1 0 bot
	 * 0 2 top left
	 * 2 2 top right
	 * 0 0 bot left
	 * 0 2 bot right
	 */
	public List<Object>[][] getSurroundingAsArray(Shape shape, float margin){
		return getSurroundingAsArray(shape, margin, names().toArray(new String[0]));
	}
	/**
	 * 
	 * @param shape
	 * @param margin
	 * @param filter
	 * @param worlds
	 * @return two dim array with lists of objects surrounding the given shape<br>
	 * 0 1 left
	 * 2 1 right
	 * 1 2 top
	 * 1 0 bot
	 * 0 2 top left
	 * 2 2 top right
	 * 0 0 bot left
	 * 0 2 bot right
	 */
	public List<Object>[][] getSurroundingAsArray(Shape shape, float margin, String...worldNames){
		@SuppressWarnings("unchecked")
		List<Object>[][] array = new List[3][3];
		
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				array[i][j] = new LinkedList<Object>();
			}
		}
		
		for(String world : worldNames) {
			@SuppressWarnings("unchecked")
			WorldAdapter<?, ?, ?> adapter = get(world, WorldAdapter.class);
			
			if(adapter == null)
				continue;
			if(!(adapter instanceof Surrounding))
				continue;
			
			Surrounding surrounding = (Surrounding) adapter;
			
			surrounding.getSurroundingAsArray(shape, margin, array);
		}
		
		return array;
	}
}
