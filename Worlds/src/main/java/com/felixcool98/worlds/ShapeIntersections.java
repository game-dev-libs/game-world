package com.felixcool98.worlds;

import java.util.List;

import com.felixcool98.aabb.Shape;

/**
 * allows retrieving all objects overlapping a shape
 * 
 * @author felixcool98
 */
public interface ShapeIntersections {
	public List<Object> getIntersections(Shape shape);
}
