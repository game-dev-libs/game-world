package com.felixcool98.worlds;

import java.util.List;

/**
 * allows retrieving all objects at a given point
 * 
 * @author felixcool98
 */
public interface PointIntersections {
	public List<Object> getIntersections(float x, float y);
}
