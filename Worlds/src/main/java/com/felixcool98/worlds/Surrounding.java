package com.felixcool98.worlds;

import java.util.List;

import com.felixcool98.aabb.Shape;

public interface Surrounding {
	/**
	 * 
	 * @param shape
	 * @param margin
	 * @param filter
	 * @param worlds
	 * @return two dim array with lists of objects surrounding the given shape<br>
	 * 0 1 left
	 * 2 1 right
	 * 1 2 top
	 * 1 0 bot
	 * 0 2 top left
	 * 2 2 top right
	 * 0 0 bot left
	 * 0 2 bot right
	 */
	public void getSurroundingAsArray(Shape shape, float margin, List<Object>[][] array);
}
