package com.felixcool98.step;

public abstract class CastedStepMethod<T> implements StepMethod {
	private Class<T> classs;
	
	
	public CastedStepMethod(Class<T> classs) {
		this.classs = classs;
	}


	@SuppressWarnings("unchecked")
	@Override
	public final void step(Object object, float delta) {
		castedStep((T) object, delta);
	}
	
	
	public abstract void castedStep(T object, float delta);
	
	
	@Override
	public boolean valid(Object object) {
		return classs.isAssignableFrom(object.getClass());
	}
}
