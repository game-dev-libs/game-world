package com.felixcool98.step;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class StepMethodWrapper implements StepMethod {
	private Method method;
	private Step step;
	
	
	public StepMethodWrapper(Method method, Step step) {
		method.setAccessible(true);
		
		this.method = method;
		this.step = step;
	}
	
	
	@Override
	public String group() {
		return step.group();
	}
	@Override
	public boolean parallel() {
		return step.parallel();
	}
	@Override
	public int priority() {
		return step.priority();
	}
	@Override
	public void step(Object object, float delta) {
		try {
			method.invoke(object, delta);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public String toString() {
		return method.toString();
	}
}
