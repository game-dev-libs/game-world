package com.felixcool98.step;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
public @interface Step {
	public String group() default "default";
	public boolean parallel() default false;
	public int priority() default 0;
}
