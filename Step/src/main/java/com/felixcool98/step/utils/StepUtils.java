package com.felixcool98.step.utils;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

import com.felixcool98.step.Step;
import com.felixcool98.step.StepMethod;
import com.felixcool98.step.StepMethodWrapper;

public class StepUtils {
	/**
	 * turns all methods in the given class with the {@link Step} annotation into StepMethods
	 * 
	 * @param classs
	 * @return
	 */
	public static List<StepMethod> getStepMethods(Class<?> classs){
		List<StepMethod> methods = new LinkedList<StepMethod>();
		
		for(Method method : classs.getMethods()) {
			if(!method.isAnnotationPresent(Step.class))
				continue;
			
			if(method.getParameterCount() != 1 || !method.getParameterTypes()[0].equals(float.class))
				continue;
			
			Step step = method.getAnnotation(Step.class);
			
			methods.add(new StepMethodWrapper(method, step));
		}
		
		return methods;
	}
}
