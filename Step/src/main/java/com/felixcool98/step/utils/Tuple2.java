package com.felixcool98.step.utils;

public class Tuple2<A, B> {
	public A a;
	public B b;
	
	
	public Tuple2() {}
	public Tuple2(A a, B b) {
		this.a = a;
		this.b = b;
	}
}
