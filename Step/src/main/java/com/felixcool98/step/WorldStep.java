package com.felixcool98.step;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import com.felixcool98.step.utils.Tuple2;

public class WorldStep {
	private ExecutorService executor;
	
	private Consumer<Object> afterStep;
	
	
	public WorldStep() {
		this(Executors.newFixedThreadPool(2));
	}
	public WorldStep(ExecutorService executor) {
		this.executor = executor;
	}
	
	
	public void setAfterStep(Consumer<Object> afterStep) {
		this.afterStep = afterStep;
	}
	
	
	public void step(List<Tuple2<Object, StepMethod>> objects, float delta) {
		Map<Integer, Map<String, List<Tuple2<Object, StepMethod>>>> nonParallel = new HashMap<Integer, Map<String,List<Tuple2<Object,StepMethod>>>>();
		Map<Integer, Map<String, List<Tuple2<Object, StepMethod>>>> parallel = new HashMap<Integer, Map<String,List<Tuple2<Object,StepMethod>>>>();
		
		for(Tuple2<Object, StepMethod> tuple : objects) {
			if(tuple.b.parallel()) {//shold be executed parallel
				if(!parallel.containsKey(tuple.b.priority()))
					parallel.put(tuple.b.priority(), new HashMap<String, List<Tuple2<Object,StepMethod>>>());
				
				Map<String, List<Tuple2<Object,StepMethod>>> priority = parallel.get(tuple.b.priority());
				
				if(!priority.containsKey(tuple.b.group()))
					priority.put(tuple.b.group(), new LinkedList<Tuple2<Object,StepMethod>>());
				
				priority.get(tuple.b.group()).add(tuple);
			}else {//should be executed after each other
				if(!nonParallel.containsKey(tuple.b.priority()))
					nonParallel.put(tuple.b.priority(), new HashMap<String, List<Tuple2<Object,StepMethod>>>());
				
				Map<String, List<Tuple2<Object,StepMethod>>> priority = nonParallel.get(tuple.b.priority());
				
				if(!priority.containsKey(tuple.b.group()))
					priority.put(tuple.b.group(), new LinkedList<Tuple2<Object,StepMethod>>());
				
				priority.get(tuple.b.group()).add(tuple);
			}
		}
		
		HashSet<Integer> priorities = new LinkedHashSet<Integer>();
		
		priorities.addAll(nonParallel.keySet());
		priorities.addAll(parallel.keySet());
		
		for(Integer priority : priorities) {
			Map<String, List<Tuple2<Object, StepMethod>>> currentNP = nonParallel.get(priority);
			Map<String, List<Tuple2<Object, StepMethod>>> currentP = parallel.get(priority);
			
			HashSet<String> groups = new HashSet<String>();
			
			if(currentNP != null) 
				groups.addAll(currentNP.keySet());
			if(currentP != null) 
				groups.addAll(currentP.keySet());
			
			for(String group : groups) {
				if(currentNP != null) {
					for(Tuple2<Object, StepMethod> tuple : currentNP.get(group)) 
						tuple.b.step(tuple.a, delta);
					
					if(afterStep != null) {
						for(Tuple2<Object, StepMethod> tuple : currentNP.get(group)) 
							afterStep.accept(tuple.a);
					}
				}
				if(currentP != null) {
					executeParallel(currentP.get(group), delta);
					
					if(afterStep != null) {
						for(Tuple2<Object, StepMethod> tuple : currentNP.get(group)) 
							afterStep.accept(tuple.a);
					}
				}
			}
		}
	}
	
	
	private void executeParallel(List<Tuple2<Object, StepMethod>> methods, float delta) {
		List<Callable<Void>> tasks = new LinkedList<Callable<Void>>();
		
		
		for(Tuple2<Object, StepMethod> tuple : methods) {
			tasks.add(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					tuple.b.step(tuple.a, delta);
					
					return null;
				}
			});
		}
		
		try {
			executor.invokeAll(tasks);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
