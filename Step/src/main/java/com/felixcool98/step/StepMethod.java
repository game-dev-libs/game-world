package com.felixcool98.step;

public interface StepMethod {
	public default String group() {
		return "default";
	}
	public default int priority() {
		return 0;
	}
	public default boolean parallel() {
		return false;
	}
	
	
	public void step(Object object, float delta);
	
	
	public default boolean valid(Object object) {
		return true;
	}
}
